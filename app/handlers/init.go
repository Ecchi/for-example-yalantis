package handlers

import (
	"udhtu-api/app/helpers"
	"udhtu-api/app/packages/authentification"
	"udhtu-api/app/packages/questionnaire"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	validator "gopkg.in/go-playground/validator.v9"
)

type (
	Handler struct {
		Authentification *authentification.Authentification
		Questionnaire    *questionnaire.Questionnaire
		Echo             *echo.Echo
	}
)

func NewHandler(
	authentification *authentification.Authentification, questionnaire *questionnaire.Questionnaire) *Handler {
	h := &Handler{
		Echo:             echo.New(),
		Authentification: authentification,
		Questionnaire:    questionnaire,
	}

	h.Echo.Validator = helpers.NewValidator()
	h.Echo.Logger.SetLevel(log.DEBUG)
	h.Echo.Pre(middleware.RemoveTrailingSlash())
	h.Echo.Use(middleware.Logger())
	h.Echo.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))
	// h.Echo.Use(middleware.StaticWithConfig(middleware.StaticConfig{
	// 	Root:   "client/dist",
	// 	Index:  "index.html",
	// 	HTML5:  true,
	// 	Browse: false,
	// }))

	// v1.Use(echoMiddleware.StaticWithConfig(echoMiddleware.StaticConfig{
	// 	Root:   "client_store/",
	// 	Index:  "index.html",
	// 	HTML5:  true,
	// 	Browse: false,
	// }))

	// v1.Use(echoMiddleware.StaticWithConfig(echoMiddleware.StaticConfig{
	// 	Root:   "client_control_panel/",
	// 	Index:  "index.html",
	// 	HTML5:  true,
	// 	Browse: false,
	// }))

	return h
}

func NewValidator() *Validator {
	return &Validator{
		validator: validator.New(),
	}
}

type Validator struct {
	validator *validator.Validate
}

func (v *Validator) Validate(i interface{}) error {
	return v.validator.Struct(i)
}
