package handlers

import (
	"udhtu-api/app/helpers"
	jwtmiddleware "udhtu-api/app/middleware"

	"github.com/labstack/echo"
)

func (h *Handler) Register(api *echo.Group) {
	jwtMiddleware := jwtmiddleware.JWT(helpers.JWTSecret)
	guestUsers := api.Group("/users")
	guestUsers.POST("/login", h.Authentification.Login)
	guestUsers.POST("/register", h.Authentification.CreateUser)

	user := api.Group("/user", jwtMiddleware)
	user.GET("", h.Authentification.Handler.CurrentUser)

	questionnaires := api.Group("/questionnaires", jwtMiddleware)
	questionnaires.GET("/:id", h.Questionnaire.GetQuestionnaireByID)
	questionnaires.GET("", h.Questionnaire.GetListQuestionnaires)
	questionnaires.PUT("", h.Questionnaire.UpdateQuestionnaire)
	questionnaires.POST("", h.Questionnaire.CreateQuestionnaire)

	questionnaireAnswers := questionnaires.Group("/answers", jwtMiddleware)
	questionnaireAnswers.GET("/:id", h.Questionnaire.GetAnswersUser)
	questionnaireAnswers.POST("", h.Questionnaire.CreateAnswerUser)
	questionnaireAnswers.PUT("", h.Questionnaire.UpdateAnswerUser)

	return
}
