package helpers

import (
	"log"
	config "udhtu-api/app/config/interfaces"

	"github.com/spf13/viper"
)

func GetConfig() *config.Configuration {
	v := viper.New()
	v.SetConfigName("data/config/config")
	v.AddConfigPath(".")
	cfg := &config.Configuration{}
	if err := v.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err := v.Unmarshal(cfg)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	// log.Printf("database uri is %s", cfg)
	// log.Printf("port for this application is %d", configuration.Server.Port)
	return cfg
}
