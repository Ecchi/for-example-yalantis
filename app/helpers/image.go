package helpers

import (
	"bytes"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
	"os"
	"strings"

	"github.com/nfnt/resize"
)

// ResizeImage Изменяет размер изображения
func ResizeImage(inimage, outimage string, size int) bool {
	//fmt.Println("resizeImage")
	//fmt.Println("outimage", outimage)
	file, err := os.Open(inimage)
	if err != nil {
		//fmt.Println("os.Open", err.Error())
		return false
	}

	defer file.Close()

	if strings.HasSuffix(file.Name(), ".jpg") || strings.HasSuffix(file.Name(), ".jpeg") || strings.HasSuffix(file.Name(), ".png") {
		var img image.Image

		img, imgType, err := image.Decode(file)
		if err != nil {
			//fmt.Println("image decode", err.Error())
			return false
		}
		//fmt.Println("file image", file.Name(), "image type", imgType)

		// create buffer
		buff := new(bytes.Buffer)

		// encode image to buffer
		if imgType == "jpeg" {
			err = jpeg.Encode(buff, img, &jpeg.Options{1})
			if err != nil {
				fmt.Println("failed to create buffer", err)
			}
		} else if imgType == "png" {
			err = png.Encode(buff, img)
			if err != nil {
				fmt.Println("failed to create buffer", err)
			}
		} else {
			fmt.Println("Неизвестный формат изображения")
		}

		// convert buffer to reader
		reader := bytes.NewReader(buff.Bytes())

		imageConfig, _, err := image.DecodeConfig(reader)
		if err != nil {
			fmt.Fprintf(os.Stderr, "image.DecodeConfig %s: %v\n", inimage, err)
		}

		var width uint
		var height uint
		var positionW int
		var positionH int
		if imageConfig.Width > imageConfig.Height {
			width = uint(size)
		} else {
			height = uint(size)
		}

		// resize to width size using Lanczos resampling
		// and preserve aspect ratio
		m := resize.Resize(width, height, img, resize.Lanczos3)

		// encode image to buffer
		err = png.Encode(buff, m)
		if err != nil {
			fmt.Println("failed to create buffer", err)
		}

		// create buffer
		buffM := new(bytes.Buffer)
		// encode image to buffer
		if imgType == "jpeg" {
			err = jpeg.Encode(buffM, m, &jpeg.Options{1})
			if err != nil {
				fmt.Println("failed to create buffer", err)
			}
		} else if imgType == "png" {
			err = png.Encode(buffM, m)
			if err != nil {
				fmt.Println("failed to create buffer", err)
			}
		}
		// convert buffer to reader
		readerM := bytes.NewReader(buffM.Bytes())

		imageConfigM, _, err := image.DecodeConfig(readerM)
		if err != nil {
			fmt.Println("image.DecodeConfig", err.Error())
		}

		if imageConfigM.Width > imageConfigM.Height {
			positionH = ((size - imageConfigM.Height) / 2) * -1
		} else {
			positionW = ((size - imageConfigM.Width) / 2) * -1
		}

		//fmt.Println("imageConfigM.Width", imageConfigM.Width, "imageConfigM.Height", imageConfigM.Height, "positionW", positionW, "positionH", positionH)

		var newimg = image.NewRGBA(image.Rect(0, 0, size, size))
		//var col color.Color

		white := color.RGBA{255, 255, 255, 255}

		// backfill entire surface with green
		draw.Draw(newimg, newimg.Bounds(), &image.Uniform{white}, image.ZP, draw.Src)

		//draw.Draw(newimg, newimg.Bounds(), m, image.Point{positionH, positionW}, draw.Src)
		draw.Draw(newimg, newimg.Bounds(), m, image.Point{positionW, positionH}, draw.Over)

		//draw.DrawMask(newimg, newimg.Bounds(), m, image.Point{positionH, positionW}, nil, image.Point{0, 100}, draw.Src)

		out, err := os.Create(outimage)
		if err != nil {
			fmt.Println(err.Error())
			return false
		}
		defer out.Close()
		if imgType == "jpeg" {
			jpeg.Encode(out, newimg, nil)
		} else if imgType == "png" {
			png.Encode(out, newimg)
		}
	} else {
		return false
	}

	return true
}

// GetImageConfig Возвращает конфиг изображения
func GetImageConfig(imageFile string) (image.Config, error) {
	fmt.Println("getImageConfig")
	var imageConfig image.Config
	file, err := os.Open(imageFile)
	if err != nil {
		fmt.Println("os.Open", err.Error())
		return imageConfig, err
	}
	defer file.Close()

	if strings.HasSuffix(file.Name(), ".jpg") || strings.HasSuffix(file.Name(), ".jpeg") || strings.HasSuffix(file.Name(), ".png") {
		var img image.Image
		/*img, err := jpeg.Decode(file)
		if err != nil {
			img, err = png.Decode(file)
			if err != nil {
				fmt.Println(err.Error())
				return imageConfig, err
			}
		}*/

		img, imgType, err := image.Decode(file)
		if err != nil {

			fmt.Println("image.Decode", err.Error())
			return imageConfig, err
		}

		//fmt.Println("file image", file.Name(), "image type", imgType)

		// create buffer
		buff := new(bytes.Buffer)

		// encode image to buffer
		if imgType == "jpeg" {
			err = jpeg.Encode(buff, img, &jpeg.Options{1})
			if err != nil {
				fmt.Println("failed to create buffer", err)
			}
		} else if imgType == "png" {
			err = png.Encode(buff, img)
			if err != nil {
				fmt.Println("failed to create buffer", err)
				return imageConfig, err
			}
		}

		// convert buffer to reader
		reader := bytes.NewReader(buff.Bytes())

		imageConfig, _, err := image.DecodeConfig(reader)
		if err != nil {
			//fmt.Fprintf(os.Stderr, "image.DecodeConfig %s: %v\n", imageFile, err)
			fmt.Println("image.DecodeConfig", err.Error())
		}

		return imageConfig, nil
	}

	return imageConfig, nil
}
