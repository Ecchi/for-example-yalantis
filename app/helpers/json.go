package helpers

import (
	"encoding/json"
	"fmt"
	"os"
)

// SaveJSONFile сохранение JSON
func SaveJSONFile(v interface{}, path string) error {
	fo, err := os.Create(path)
	if err != nil {
		return fmt.Errorf("JSON create patch err ", err.Error())
	}
	defer fo.Close()
	e := json.NewEncoder(fo)
	if err := e.Encode(v); err != nil {
		return fmt.Errorf("JSON encode err", err.Error())
	}
	return nil
}

// ReadJSONFile Чтение JSON
func ReadJSONFile(v interface{}, path string) error {
	fo, err := os.Open(path)
	if err != nil {
		//panic(err)
		return fmt.Errorf("file not found")
	}
	defer fo.Close()

	e := json.NewDecoder(fo)
	if err := e.Decode(v); err != nil {
		return fmt.Errorf("JSON save err", err.Error())
	}

	//fmt.Println("json", v)
	return nil
}
