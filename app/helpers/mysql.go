package helpers

import (
	config "udhtu-api/app/config/interfaces"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func ConnectDB(config *config.Configuration) (DB *gorm.DB, err error) {
	conn := config.Database.Username + ":" + config.Database.Password + "@tcp(" + config.Database.Host + ":" + config.Database.Port + ")/" + config.Database.Database + "?charset=utf8&parseTime=True&loc=Local"
	DB, err = gorm.Open("mysql", conn)
	return
}
