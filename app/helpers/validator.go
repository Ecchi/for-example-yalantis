package helpers

import (
	validator "gopkg.in/go-playground/validator.v9"
)

// CustomValidator
type CustomValidator struct {
	validator *validator.Validate
}

// NewValidator
func NewValidator() *CustomValidator {
	return &CustomValidator{validator: validator.New()}

}

// Validate validate
func (cv *CustomValidator) Validate(i interface{}) error {
	// cv.validator.RegisterValidation("hogehoge", CustomValidate)
	return cv.validator.Struct(i)
}

// // CustomValidate
// func CustomValidate(fl validator.FieldLevel) bool {
// 	return fl.Field().String() == "hogehoge"
// }

// Foo struct
// type Foo struct {
// 	Bar string `query:"bar" validate:"required,hogehoge"`
// }
