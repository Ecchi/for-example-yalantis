package handlers

import (
	"fmt"
	"net/http"
	"udhtu-api/app/helpers"
	"udhtu-api/app/packages/authentification/models"
	"udhtu-api/app/packages/authentification/usecases/interfaces"

	"github.com/labstack/echo"
)

type (
	Handler struct {
		interfaces.Usecase
	}
)

func NewHandler(Usecase interfaces.Usecase) *Handler {
	h := &Handler{
		Usecase,
	}

	return h
}
func (h *Handler) CreateUser(c echo.Context) (err error) {
	userRegister := new(models.UserRegister)
	// x, _ := ioutil.ReadAll(c.Request().Body)
	// fmt.Printf("-------------- %s", string(x))
	userRegister.Email = "mugiwar@gmail.com"
	// userRegister.Phone = "893222323232"
	// // userRegister.UserData = []
	userRegister.Password = "123456"
	userRegister.Login = "Nikals"
	if err := c.Bind(userRegister); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}
	if err := c.Validate(userRegister); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}
	fmt.Println("userRegister", userRegister)
	u, err := h.Usecase.CreateUser(userRegister)
	if err != nil {
		fmt.Printf("%+v", err)
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}
	return c.JSON(http.StatusOK, u)
}
func (h *Handler) Login(c echo.Context) (err error) {
	var user models.User
	// if err := c.Bind(userRegister); err != nil {
	// 	return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	// }
	// if err := c.Validate(userRegister); err != nil {
	// 	return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	// }
	user.Email = "mugiwar@gmail.com"
	user.Password = "123456"
	u, err := h.Usecase.GetUser(user.Email)
	fmt.Printf("%+v", u)
	if err != nil {

		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}
	if u == nil {
		return c.JSON(http.StatusForbidden, helpers.AccessForbidden())
	}
	if !u.CheckPassword(user.Password) {
		return c.JSON(http.StatusForbidden, helpers.AccessForbidden())
	}
	// u.Password = helpers.GenerateJWT(u.ID)
	type Responce struct {
		Token string `json:"token"`
	}
	fmt.Println("-----=-=-=-=", helpers.GenerateJWT(u.ID))
	r := Responce{
		Token: helpers.GenerateJWT(u.ID),
	}
	return c.JSON(http.StatusOK, r)
}
func (h *Handler) CurrentUser(c echo.Context) error {
	// u, err := h.userStore.GetByID(userIDFromToken(c))
	// if err != nil {
	// 	return c.JSON(http.StatusInternalServerError, utils.NewError(err))
	// }
	// if u == nil {
	// 	return c.JSON(http.StatusNotFound, utils.NotFound())
	// }
	type Responce struct {
		ID   uint   `json:"id"`
		Role string `json:"roles"`
		Name string `json:"name"`
	}
	r := Responce{
		ID:   userIDFromToken(c),
		Role: "admin",
		Name: "NIkals",
	}
	return c.JSON(http.StatusOK, r)
}
func userIDFromToken(c echo.Context) uint {
	id, ok := c.Get("user").(uint)
	if !ok {
		return 0
	}
	return id
}
