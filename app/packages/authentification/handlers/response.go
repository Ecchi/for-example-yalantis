package handlers

// import "gitlab.com/thedartem/gostore/app/packages/product/models"

// type productResponse struct {
// 	Product struct {
// 		ID              uint           `json:"id"`
// 		Title           string         `json:"title"`
// 		MetaTitle       string         `json:"meta_title"`
// 		MetaDescription string         `json:"meta_description"`
// 		Description     string         `json:"description"`
// 		URL             string         `json:"url"`
// 		Images          []productImage `json:"images"`
// 		Status          bool           `json:"status"`
// 	} `json:"product"`
// }

// type productImage struct {
// 	Server   string `json:"server"`
// 	Filename string `json:"filename"`
// 	IsMain   bool   `json:"is_main"`
// }

// func newProductResponse(m *models.Product) *productResponse {
// 	r := new(productResponse)
// 	r.Product.ID = m.ID
// 	r.Product.Title = m.Title
// 	r.Product.MetaTitle = m.MetaTitle
// 	r.Product.MetaDescription = m.MetaDescription
// 	r.Product.Description = m.Description
// 	r.Product.URL = m.URL
// 	r.Product.Status = m.Status

// 	var productImages []productImage
// 	for _, item := range m.Images {
// 		var img productImage

// 		img.Server = item.Server
// 		img.Filename = item.Filename
// 		img.IsMain = item.IsMain

// 		productImages = append(productImages, img)
// 	}

// 	r.Product.Images = productImages

// 	return r
// }

// func newProductsResponse(m *[]models.Product) []*productResponse {
// 	var (
// 		ps []*productResponse
// 	)

// 	for _, item := range *m {
// 		r := new(productResponse)
// 		r.Product.ID = item.ID
// 		r.Product.Title = item.Title
// 		r.Product.MetaTitle = item.MetaTitle
// 		r.Product.MetaDescription = item.MetaDescription
// 		r.Product.Description = item.Description
// 		r.Product.URL = item.URL
// 		r.Product.Status = item.Status

// 		var productImages []productImage
// 		for _, item := range item.Images {
// 			var img productImage

// 			img.Server = item.Server
// 			img.Filename = item.Filename
// 			img.IsMain = item.IsMain

// 			productImages = append(productImages, img)
// 		}

// 		r.Product.Images = productImages

// 		ps = append(ps, r)
// 	}

// 	return ps
// }
