package authentification

import (
	"fmt"
	"udhtu-api/app/packages/authentification/handlers"
	repositories "udhtu-api/app/packages/authentification/repositories/gorm"
	"udhtu-api/app/packages/authentification/usecases"

	"github.com/jinzhu/gorm"
)

type (
	Authentification struct {
		*handlers.Handler
	}
)

func New(db *gorm.DB) (*Authentification, error) {
	rep, err := repositories.New(db)
	if err != nil {
		fmt.Println("Init Authentification repositories err", err)
	}
	usc := usecases.New(rep)
	if err != nil {
		fmt.Println("Init Authentification Usecases err", err)
	}

	return &Authentification{handlers.NewHandler(usc)}, err
}
