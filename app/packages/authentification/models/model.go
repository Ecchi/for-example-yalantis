package models

import (
	"errors"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type (
	UsersData struct {
		UserID   uint       `gorm:"column:user_id;"`
		User     User       `gorm:"foreignkey:id;association_foreignkey:user_id"`
		UserData []UserData `gorm:"foreignkey:id;association_foreignkey:user_id"`
	}
	User struct {
		ID         uint       `json:"id,omitempty" gorm:"primary_key;AUTO_INCREMENT"`
		Password   string     `json:"password" validate:"required,min=6,max=100"`
		Login      string     `json:"login" gorm:"column:login;type:varchar(100);unique; not null" validate:"required,min=5,max=100"`
		Email      string     `json:"email" gorm:"column:email;type:varchar(100);unique; not null" validate:"required,email"`
		Phone      string     `json:"phone" gorm:"column:phone;type:varchar(50);unique; not null" validate:"required"`
		UserData   []UserData `json:"user_data" form:"user_data" query:"user_data" gorm:"foreignkey:user_id;auto_preload"`
		UserRoleID uint       `gorm:"column:user_role_id;type:int unsigned;"`
		Status     int        `gorm:"column:status_id;type:int;"`
		CreatedAt  time.Time  `gorm:"column:created_at; not null"`
		UpdatedAt  time.Time  `gorm:"column:update_at; not null"`
	}
	UserRecovery struct {
		ID        uint      `json:"id,omitempty" gorm:"primary_key;AUTO_INCREMENT"`
		UserID    uint      `json:"user_id" form:"user_id" validate:"gte=0,required"`
		Hash      string    `json:"name" validate:"required,max=100"`
		Status    int       `gorm:"column:status_id;type:int;"` // востановлен или нет итп
		CreatedAt time.Time `gorm:"column:created_at; not null"`
		UpdatedAt time.Time `gorm:"column:update_at; not null"`
	}
	UserDataType struct {
		ID   uint   `json:"id,omitempty" gorm:"primary_key;AUTO_INCREMENT"`
		Name string `json:"name" validate:"required,min=2,max=100"` // phone email Lname Fname
	}
	UserRole struct {
		ID   uint   `json:"id,omitempty" gorm:"primary_key;AUTO_INCREMENT"`
		Name string `json:"name" validate:"required,min=2,max=100"` //
	}
	UserData struct {
		ID             uint   `json:"id,omitempty" gorm:"primary_key;AUTO_INCREMENT"`
		UserID         uint   `json:"user_id" form:"user_id" validate:"gte=0,required"`
		UserDataTypeID uint   `json:"user_data_type_id" form:"user_data_type_id" validate:"gte=0,required"`
		Data           string `json:"data" gorm:"column:data;type:text;"`
		Default        int
		Status         int       `gorm:"column:status_id;type:int;"` // видно всем или только тебе
		CreatedAt      time.Time `gorm:"column:created_at; not null"`
		UpdatedAt      time.Time `gorm:"column:update_at; not null"`
	}
)

func (u *User) HashPassword(plain string) (string, error) {
	if len(plain) == 0 {
		return "", errors.New("password should not be empty")
	}
	h, err := bcrypt.GenerateFromPassword([]byte(plain), bcrypt.DefaultCost)
	return string(h), err
}

func (u *User) CheckPassword(plain string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(plain))
	return err == nil
}
