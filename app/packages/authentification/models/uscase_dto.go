package models

type (
	UserRegister struct {
		Password string     `json:"password" validate:"required,min=6,max=100"`
		Login    string     `json:"login" gorm:"column:login;type:varchar(100);unique; not null" validate:"required,min=5,max=100"`
		Email    string     `json:"email" gorm:"column:email;type:varchar(50);unique; not null" validate:"required,email"`
		Phone    string     `json:"phone" gorm:"column:phone;type:varchar(50);unique; not null" validate:"required"`
		UserData []UserData `json:"user_data,omitempty" form:"user_data" query:"user_data" gorm:"foreignkey:user_id;auto_preload"`
	}
)
