package gorm

// import (
// 	"udhtu/core/packages/authentification/models"

// 	"github.com/pkg/errors"
// )

// func (repo *Repository) CreatePlan(plan models.Plan) (err error) {
// 	err = repo.DB.Create(&plan).Error
// 	if err != nil {
// 		return errors.Errorf("plan небыл создан - %s", err)
// 	}
// 	return
// }

// func (repo *Repository) GetPlans(plan models.Plan) (plans []models.Plan, err error) {
// 	err = repo.DB.Where(&plan).Find(&plans).Error
// 	if err != nil {
// 		return nil, errors.Errorf("Возникла ошибка репозитария GetPlans - %s", err)
// 	}
// 	return
// }

// func (repo *Repository) CreatePlanQuestion(planQuestion models.PlanQuestion) (err error) {
// 	err = repo.DB.Create(&planQuestion).Error
// 	if err != nil {
// 		return errors.Errorf("planQuestion небыл создан - %s", err)
// 	}
// 	return
// }

// func (repo *Repository) GetPlanQuestions(planQuestion models.PlanQuestion) (planQuestions []models.PlanQuestion, err error) {
// 	err = repo.DB.Where(&planQuestion).Find(&planQuestions).Error
// 	if err != nil {
// 		return nil, errors.Errorf("Возникла ошибка репозитария planQuestions - %s", err)
// 	}
// 	return
// }
// func (repo *Repository) CreateQuestionTagType(questionType models.QuestionTagType) (err error) {
// 	err = repo.DB.Create(&questionType).Error
// 	if err != nil {
// 		return errors.Errorf("questionType небыл создан - %s", err)
// 	}
// 	return
// }

// func (repo *Repository) GetQuestionTagTypes(questionTagType models.QuestionTagType) (questionTagTypes []models.QuestionTagType, err error) {
// 	err = repo.DB.Where(&questionTagType).Find(&questionTagTypes).Error
// 	if err != nil {
// 		return nil, errors.Errorf("Возникла ошибка репозитария questionTagTypes - %s", err)
// 	}
// 	return
// }
// func (repo *Repository) CreateQuestion(question models.Question) (err error) {
// 	err = repo.DB.Create(&question).Error
// 	if err != nil {
// 		return errors.Errorf("question небыл создан - %s", err)
// 	}
// 	return
// }

// func (repo *Repository) GetQuestions(question models.Question) (questions []models.Question, err error) {
// 	err = repo.DB.Where(&question).Find(&questions).Error
// 	if err != nil {
// 		return nil, errors.Errorf("Возникла ошибка репозитария questions - %s", err)
// 	}
// 	return
// }
// func (repo *Repository) CreateAnswer(answer models.Answer) (err error) {
// 	err = repo.DB.Create(&answer).Error
// 	if err != nil {
// 		return errors.Errorf("answer небыл создан - %s", err)
// 	}
// 	return
// }

// func (repo *Repository) CreateAnswerUser(answersUser models.AnswersUser) (err error) {
// 	err = repo.DB.Create(&answersUser).Error
// 	if err != nil {
// 		return errors.Errorf("answersUser небыл создан - %s", err)
// 	}
// 	return
// }

// // // ---
// // func (repo *Repository) GetAccount(findInvoice models.Invoice) (account models.Account, err error) {
// // 	// err = repo.DB.Table("transactions").Select("user_id,sum(amount) as amount").Where("user_id = ?", userID).Find(&account).Error
// // 	// err = repo.DB.Model(&tr).Related(&invoice).Error
// // 	// err = repo.DB.Table("transactions").Select("user_id,sum(amount) as amount,invoice_id").Where("invoice_id = ?", invoiceID).Preload("Invoice").Find(&account).Error
// // 	err = repo.DB.Table("transactions").Select("sum(amount) as amount,invoice_id").Where("invoice_id = ?", repo.DB.Table("invoices").Select("id").Where(&findInvoice).SubQuery()).Preload("Invoice").Find(&account).Error
// // 	if err != nil {
// // 		return account, errors.Errorf("Возникла ошибка репозитария GetAccountByInvoiceID - %s", err)
// // 	}
// // 	if account.Invoice.ID <= 0 {
// // 		return account, errors.New("InvoiceID не найден в таблице транзакций")
// // 	}

// // 	return
// // }

// // // Трансвер средст со счета на счет
// // func (repo *Repository) RegisterTransferTransaction(fromTransaction models.Transaction, toTransaction models.Transaction) (err error) {
// // 	tx := repo.DB.Begin()
// // 	defer func() {
// // 		if r := recover(); r != nil {
// // 			tx.Rollback()
// // 		}
// // 	}()
// // 	if tx.Error != nil {
// // 		return err
// // 	}
// // 	if err := tx.Create(&fromTransaction).Error; err != nil {
// // 		tx.Rollback()
// // 		return err
// // 	}
// // 	if err := tx.Create(&toTransaction).Error; err != nil {
// // 		tx.Rollback()
// // 		return err
// // 	}
// // 	return tx.Commit().Error
// // }

// // // Изменить статус счета активен/неактивен/блокирован и т д
// // func (repo *Repository) ChangeInvoiceStatus(invoiceDTO models.ChangeInvoiceStatusDTO) (invoice models.Invoice, err error) {
// // 	err = repo.DB.Model(&invoice).Where("id = ?", invoiceDTO.ID).Update("namstatus_ide", invoiceDTO.StatusID).Error
// // 	// err = repo.DB.Model(&invoice).Where("id = ?", invoiceID).Update("namstatus_ide", statusID).Error
// // 	if err != nil {
// // 		if gorm.IsRecordNotFoundError(err) {
// // 			return invoice, err
// // 		}
// // 		return invoice, errors.Errorf("Ошибка при изменение ChangeInvoiceStatus статуса  - %s", err)
// // 	}
// // 	return

// // }
