package gorm

import (
	"udhtu-api/app/packages/authentification/models"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type (
	Repository struct {
		DB *gorm.DB
	}
)

// Создает при старте приложения соеденение , !! - если при старте не отработала повторного вызова не будет!!!!!
func New(db *gorm.DB) (*Repository, error) {
	var (
		user         models.User
		userDataType models.UserDataType
		userData     models.UserData
		userRole     models.UserRole
		err          error
	)

	if !db.HasTable(&userDataType) {
		err = db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&userDataType).Error
		if err != nil {
			return nil, errors.Errorf("Таблица puserDataTypelan не создана - %s", err)
		}
	}

	if !db.HasTable(&userRole) {
		err = db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&userRole).Error
		if err != nil {
			return nil, errors.Errorf("Таблица userRole не создана - %s", err)
		}
	}

	if !db.HasTable(&user) {
		err = db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&user).Error
		if err != nil {
			return nil, errors.Errorf("Таблица user не создана - %s", err)
		}
		err = db.Model(&user).AddIndex("idx_user_role_id", "user_role_id").AddForeignKey("user_role_id", "user_roles(id)", "RESTRICT", "RESTRICT").Error
		if err != nil {
			return nil, errors.Errorf("Внешние ключ или индексы в таблице не создались user_role_id - %s", err)
		}
	}
	if !db.HasTable(&userData) {
		err = db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&userData).Error
		if err != nil {
			return nil, errors.Errorf("Таблица userData не создана - %s", err)
		}
		err = db.Model(&userData).AddIndex("idx_user_data_type_id", "user_data_type_id").AddForeignKey("user_data_type_id", "user_data_types(id)", "RESTRICT", "RESTRICT").Error
		if err != nil {
			return nil, errors.Errorf("Внешние ключ или индексы в таблице не создались user_data_type_id - %s", err)
		}
	}
	return &Repository{DB: db}, nil
}
