package interfaces

import (
	"udhtu-api/app/packages/authentification/models"
)

type (
	Repository interface {
		// User
		CreateUser(user *models.User) (err error)
		GetUser(user models.User) (u *models.User, err error)
		GetUsersDatas(user models.User) (usersData []models.UsersData, err error)
	}
)
