package interfaces

import (
	"udhtu-api/app/packages/authentification/models"
)

type (
	Usecase interface {
		//jwt
		// CheckJwt(next echo.HandlerFunc) echo.HandlerFunc
		// User
		CreateUser(user *models.UserRegister) (u *models.User, err error)
		GetUser(email string) (u *models.User, err error)
		// Login
		// Login(user models.User) (token string, err error)
		// Хеширование и проверка пароля

		// CreatePlanQuestion(planQuestion models.PlanQuestion) (err error)
		// GetPlanQuestions(planQuestion models.PlanQuestion) (planQuestions []models.PlanQuestion, err error)
		// // QuestionType
		// CreateQuestionTagType(questionType models.QuestionTagType) (err error)
		// GetQuestionTagTypes(questionType models.QuestionTagType) (questionTypes []models.QuestionTagType, err error)
		// // Question
		// CreateQuestion(question models.Question) (err error)
		// GetQuestions(question models.Question) (questions []models.Question, err error)
		// // Answer
		// CreateAnswer(plan models.Answer) (err error)
		// CreateAnswerUser(answersUser models.AnswersUser) (err error)
	}
)
