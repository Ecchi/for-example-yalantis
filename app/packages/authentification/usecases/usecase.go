package usecases

import (
	"errors"
	"udhtu-api/app/packages/authentification/models"
	"udhtu-api/app/packages/authentification/repositories/gorm"
	"udhtu-api/app/packages/authentification/repositories/interfaces"
)

type (
	Usecase struct {
		interfaces.Repository
	}
)

func New(rep *gorm.Repository) *Usecase {
	return &Usecase{
		rep,
	}

}
func (usecase *Usecase) CreateUser(userRegData *models.UserRegister) (*models.User, error) {
	// config, err := utilites.ReadConfig("core/config/global")
	// if err != nil {
	// 	return transaction, errors.New("Настроики или сам файл с ними data/global отсутсвует")
	// }
	// transaction.TransactionTypeID = config.GetInt("accounting.transaction_type.deposit")
	var user models.User
	user.Email = userRegData.Email
	user.Phone = userRegData.Phone
	user.UserData = userRegData.UserData
	user.Login = userRegData.Login
	h, err := user.HashPassword(userRegData.Password)
	if err != nil {
		return nil, err
	}

	user.Password = h
	user.Status = 1
	user.UserRoleID = 1

	// user.UserData[0]. = 1
	if err := usecase.Repository.CreateUser(&user); err != nil {
		return nil, errors.New("CreateUser err")
	}

	return &user, nil
}
func (usecase *Usecase) GetUser(email string) (u *models.User, err error) {

	return usecase.Repository.GetUser(models.User{Email: email})
}

// func (usecase *Usecase) Login(user models.User) (t string, err error) {

// 	u, err := usecase.Repository.GetUsers(user)
// 	if err != nil {
// 		return
// 	}
// 	if len(u) == 1 && u[0].Status == 1 {
// 		ok := usecase.CheckPasswordHash(user.Password, u[0].Password)
// 		fmt.Println("22222----------++++++__", ok)
// 		if err != nil {
// 			return t, errors.New("Функция хеширования вернула шобку, " + err.Error())
// 		}
// 		// Set custom claims
// 		claims := &models.JwtUserClaims{
// 			u[0].ID,
// 			jwt.StandardClaims{
// 				ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
// 			},
// 		}

// 		// Create token with claims
// 		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

// 		// Generate encoded token and send it as response.
// 		t, err = token.SignedString([]byte("secret"))
// 		if err != nil {
// 			return
// 		}
// 		return
// 	}

// 	return t, errors.New("Данные не верны в базе нет")
// }

// CheckJwt проверка токена
// func (usecase *Usecase) CheckJwt(next echo.HandlerFunc) echo.HandlerFunc {
// 	return func(c echo.Context) error {

// 		user := c.Get("user").(*jwt.Token)
// 		claims := user.Claims.(*models.JwtUserClaims)
// 		userID := claims.UserID
// 		u, err := usecase.GetUsers(models.User{ID: userID})
// 		if err != nil {
// 			echo.NewHTTPError(http.StatusUnauthorized, err.Error())
// 		}
// 		if len(u) != 1 && u[0].Status != 1 {
// 			if err != nil {
// 				echo.NewHTTPError(http.StatusUnauthorized, err.Error())
// 			}
// 		}
// 		models.S.UserID = u[0].ID
// 		models.S.Login = u[0].Login
// 		// s.UserID = u[0].ID
// 		// s.Login = u[0].Login

// 		//log.Println("User Name: ", claims["name"], "User ID: ", claims["user_id"], "User ID: ", claims["jti"], "User Role: ", claims["role"])

// 		//IP := claims["ip"].(string)

// 		//if IP != c.RealIP() {
// 		//	echo.NewHTTPError(http.StatusUnauthorized, "Bad token")
// 		//}

// 		//Device := claims["device"].(string)

// 		//if Device != "" {
// 		//	echo.NewHTTPError(http.StatusUnauthorized, "Bad token")
// 		//}

// 		//log.Println("User Name: ", claims["name"], "User ID: ", claims["user_id"], "User ID: ", claims["jti"], "User Role: ", claims["role"], "IP: ", claims["IP"], "Device: ", claims["Device"])

// 		return next(c)
// 	}
// }
