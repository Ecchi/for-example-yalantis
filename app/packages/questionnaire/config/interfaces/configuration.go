package config

import (
	"log"

	"github.com/spf13/viper"
)

type Configuration struct {
	Package PackageConfiguration
}

func GetConfig() *Configuration {
	v := viper.New()
	v.SetConfigName("data/config/questionnaire/config")
	v.AddConfigPath(".")
	cfg := &Configuration{}
	if err := v.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err := v.Unmarshal(cfg)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	// log.Printf("database uri is %s", cfg)
	// log.Printf("port for this application is %d", configuration.Server.Port)
	return cfg
}
