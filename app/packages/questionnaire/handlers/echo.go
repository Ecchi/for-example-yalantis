package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"udhtu-api/app/helpers"
	"udhtu-api/app/packages/questionnaire/model"

	"udhtu-api/app/packages/questionnaire/usecases/interfaces"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

type (
	Handler struct {
		interfaces.Usecase
	}
)

func NewHandler(Usecase interfaces.Usecase) *Handler {
	h := &Handler{
		Usecase,
	}

	return h
}

func (h *Handler) UpdateQuestionnaire(c echo.Context) (err error) {
	mQuestionnaire := new(model.Questionnaire)
	// mQuestionnaire, err := h.Usecase.GetQuestionnaireByID(uint(8))
	// if err != nil {
	// 	return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	// }
	// if mQuestionnaire == nil {
	// 	return c.JSON(http.StatusNotFound, helpers.NotFound())
	// }
	req := &questionnaireUpdateRequest{}

	if err := req.bind(c, mQuestionnaire); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}

	// // fmt.Printf("%+v", err)
	err = h.Usecase.UpdateQuestionnaire(mQuestionnaire)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}

	return c.JSON(http.StatusCreated, mQuestionnaire)
}
func (h *Handler) CreateQuestionnaire(c echo.Context) (err error) {
	mQuestionnaire := new(model.Questionnaire)
	req := &questionnaireCreateRequest{}

	if err := req.bind(c, mQuestionnaire); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}
	mQuestionnaire.UserID = userIDFromToken(c)
	fmt.Printf("%+v", err)
	err = h.Usecase.CreateQuestionnaire(mQuestionnaire)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}

	return c.JSON(http.StatusCreated, newQuestionnaireResponse(c, mQuestionnaire))
}
func (h *Handler) CreateAnswerUser(c echo.Context) (err error) {
	mAnswerUser := new(model.AnswerUser)

	/*
		Status
		1- актвино
		2 - неактивно
		UserStatusID
		3 Не начато
		4 В процессе
		5 Завершино
	*/

	req := &createAnswerUserRequest{}
	if err := req.bind(c, mAnswerUser); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}
	// mQuestionnaire.ID = mAnswerUser.QuestionnaireID
	// mQuestionnaire.StatusID = 1
	mQuestionnaire, err := h.Usecase.GetQuestionnaireByID(&model.Questionnaire{
		Model:    gorm.Model{ID: mAnswerUser.QuestionnaireID},
		StatusID: 1})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	}
	if mQuestionnaire == nil {
		return c.JSON(http.StatusNotFound, helpers.NotFound())
	}
	au, err := h.Usecase.GetAnswersByUserIDByQuestionnaireID(&model.AnswerUser{
		QuestionnaireID: mQuestionnaire.ID,
		UserID:          userIDFromToken(c),
	})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	}
	if au != nil {
		return nil
	}
	if mQuestionnaire.OneTime {
		mAnswerUser.StatusID = 4
	} else {
		mAnswerUser.StatusID = 5
	}
	mAnswerUser.QuestionnaireID = mQuestionnaire.ID
	mAnswerUser.UserID = userIDFromToken(c)
	err = h.Usecase.CreateAnswerUser(mAnswerUser)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}

	return c.JSON(http.StatusCreated, mAnswerUser)
}
func (h *Handler) GetAnswersUser(c echo.Context) (err error) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	}
	au, err := h.Usecase.GetAnswersByUserIDByQuestionnaireID(&model.AnswerUser{
		QuestionnaireID: uint(id),
		UserID:          userIDFromToken(c),
	})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	}
	if au == nil {
		return nil
	}

	return c.JSON(http.StatusOK, newAnswersUserResponse(c, au))
}
func (h *Handler) UpdateAnswerUser(c echo.Context) (err error) {
	mAnswerUser := new(model.AnswerUser)

	// /*
	// 	Status
	// 	1- актвино
	// 	2 - неактивно
	// 	UserStatusID
	// 	3 Не начато
	// 	4 В процессе
	// 	5 Завершино
	// */

	req := &updateAnswerUserRequest{}
	if err := req.bind(c, mAnswerUser); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}
	// // mQuestionnaire.ID = mAnswerUser.QuestionnaireID
	// // mQuestionnaire.StatusID = 1
	// mQuestionnaire, err := h.Usecase.GetQuestionnaireByID(&model.Questionnaire{
	// 	Model:    gorm.Model{ID: mAnswerUser.QuestionnaireID},
	// 	StatusID: 1})
	// if err != nil {
	// 	return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	// }
	// if mQuestionnaire == nil {
	// 	return c.JSON(http.StatusNotFound, helpers.NotFound())
	// }

	au, err := h.Usecase.GetAnswersByUserIDByQuestionnaireID(&model.AnswerUser{
		Model:  gorm.Model{ID: mAnswerUser.ID},
		UserID: userIDFromToken(c),
	})
	if err != nil {
		return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	}
	if au == nil {
		return c.JSON(http.StatusInternalServerError, helpers.NotFound())
	}

	// if mQuestionnaire.OneTime {
	// 	mAnswerUser.StatusID = 4
	// } else {
	// 	mAnswerUser.StatusID = 5
	// }
	// mAnswerUser.QuestionnaireID = mQuestionnaire.ID
	mAnswerUser.UserID = userIDFromToken(c)

	// id, err := strconv.Atoi(c.Param("id"))
	// if err != nil {
	// 	return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	// }
	fmt.Printf("++_+_+_+_+ %+v", mAnswerUser)
	err = h.Usecase.UpdateAnswer(mAnswerUser)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, helpers.NewError(err))
	}

	return c.JSON(http.StatusCreated, mAnswerUser)
}

func (h *Handler) GetQuestionnaireByID(c echo.Context) (err error) {

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, helpers.NewError(err))
	}
	// mQuestionnaire.ID = uint(id)
	// mQuestionnaire.StatusID = 1
	mQuestionnaire, err := h.Usecase.GetQuestionnaireByID(&model.Questionnaire{
		Model:    gorm.Model{ID: uint(id)},
		StatusID: 1})
	if mQuestionnaire == nil {
		return c.JSON(http.StatusNotFound, helpers.NotFound())
	}
	return c.JSON(http.StatusOK, newQuestionnaireResponse(c, mQuestionnaire))
}
func (h *Handler) GetListQuestionnaires(c echo.Context) (err error) {
	offset, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil || offset == 1 {
		offset = 0
	}
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		limit = 20
	}
	qs, count, err := h.Usecase.GetListQuestionnaires(offset, limit)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, nil)
	}
	return c.JSON(http.StatusOK, newQuestionnaireListResponse(h.Usecase, userIDFromToken(c), qs, count))

}
func userIDFromToken(c echo.Context) uint {
	id, ok := c.Get("user").(uint)
	if !ok {
		return 0
	}
	return id
}
