package handlers

import (
	"time"
	"udhtu-api/app/packages/questionnaire/model"

	"github.com/labstack/echo"
)

// Create
type questionnaireCreateRequest struct {
	Questionnaire struct {
		Importance  uint                       `json:"importance" validate:"numeric"`
		StatusID    uint                       `json:"statusID" validate:"required,numeric,gt=0"`
		Description []descriptionCreateRequest `json:"description" validate:"dive"`
		Question    []questionCreateRequest    `json:"question,omitempty" validate:"dive"`
		Position    uint                       `json:"position"  validate:"numeric"`
		Start       time.Time                  `json:"start"  validate:""`
		End         time.Time                  `json:"end"  validate:""`
	} `json:"questionnaire"`
}
type descriptionCreateRequest struct {
	Description struct {
		Name           string `json:"name" validate:"required,min=1,max=150"`
		Description    string `json:"description" validate:"required,min=1,max=250"`
		ReportTemplate string `json:"reportTemplate" `
		LanguageID     uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}
type questionCreateRequest struct {
	Question struct {
		QuestionDescription []questionDescriptionCreateRequest `json:"description,omitempty" validate:"dive"`
		Answer              []answerCreateRequest              `json:"answer,omitempty" validate:"dive"`
		TagTypeID           uint                               `json:"tagTypeID" validate:"numeric,gt=0"`
		Position            uint                               `json:"position"  validate:"numeric"`
		StatusID            uint                               `json:"statusID" validate:"numeric,gt=0"`
	} `json:"question"`
}
type questionDescriptionCreateRequest struct {
	QuestionDescription struct {
		Name        string `json:"name" validate:"required,min=1,max=150"`
		Description string `json:"description" validate:"required,min=1,max=250"`
		LanguageID  uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}
type answerCreateRequest struct {
	Answer struct {
		AnswerDescription []answerDescriptionCreateRequest `json:"description,omitempty" validate:"dive"`
		Default           bool                             `json:"default"  `
		Position          uint                             `json:"position"  validate:"numeric"`
		Cost              float64                          `json:"cost,string"  validate:"numeric"`
		StatusID          uint                             `json:"statusID" validate:"numeric,gt=0"`
	} `json:"answer"`
}
type answerDescriptionCreateRequest struct {
	AnswerDescription struct {
		Name        string `json:"name" validate:"required,min=1,max=150"`
		Description string `json:"description" validate:"required,min=1,max=250"`
		LanguageID  uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}

func (r *questionnaireCreateRequest) bind(c echo.Context, mQuestionnaire *model.Questionnaire) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	if err := c.Validate(r); err != nil {
		return err
	}
	mQuestionnaire.Importance = r.Questionnaire.Importance
	mQuestionnaire.Start = r.Questionnaire.Start
	mQuestionnaire.End = r.Questionnaire.End
	mQuestionnaire.Position = r.Questionnaire.Position
	mQuestionnaire.StatusID = r.Questionnaire.StatusID
	if r.Questionnaire.Description != nil {
		for _, s := range r.Questionnaire.Description {
			mQuestionnaire.Description = append(mQuestionnaire.Description, model.Description{
				Name:        s.Description.Name,
				Description: s.Description.Description,
				LanguageID:  s.Description.LanguageID,
			})
		}
	}

	if r.Questionnaire.Question != nil {
		for _, q := range r.Questionnaire.Question {
			var mQuestion model.Question
			mQuestion.TagTypeID = q.Question.TagTypeID
			mQuestion.Position = q.Question.Position
			mQuestion.StatusID = q.Question.StatusID

			if q.Question.QuestionDescription != nil {
				for _, qD := range q.Question.QuestionDescription {
					mQuestion.QuestionDescription = append(mQuestion.QuestionDescription, model.QuestionDescription{
						Name:        qD.QuestionDescription.Name,
						Description: qD.QuestionDescription.Description,
						LanguageID:  qD.QuestionDescription.LanguageID,
					})
				}
			}

			if q.Question.Answer != nil {
				for _, a := range q.Question.Answer {
					var mAnswer model.Answer
					mAnswer.Default = a.Answer.Default
					mAnswer.Position = a.Answer.Position
					mAnswer.Cost = a.Answer.Cost
					mAnswer.StatusID = a.Answer.StatusID
					if a.Answer.AnswerDescription != nil {
						for _, aD := range a.Answer.AnswerDescription {
							mAnswer.AnswerDescription = append(mAnswer.AnswerDescription, model.AnswerDescription{
								Name:        aD.AnswerDescription.Name,
								Description: aD.AnswerDescription.Description,
								LanguageID:  aD.AnswerDescription.LanguageID,
							})
						}
					}
					mQuestion.Answer = append(mQuestion.Answer, mAnswer)
				}

			}

			mQuestionnaire.Question = append(mQuestionnaire.Question, mQuestion)
		}

	}
	return nil
}

//Update

type questionnaireUpdateRequest struct {
	Questionnaire struct {
		ID          uint                       `json:"id" validate:"numeric"`
		UserID      uint                       `json:"userID" validate:"numeric"`
		Importance  uint                       `json:"importance" validate:"numeric"`
		StatusID    uint                       `json:"statusID" validate:"required,numeric,gt=0"`
		Description []descriptionUpdateRequest `json:"description" validate:"dive"`
		Question    []questionUpdateRequest    `json:"question,omitempty" validate:"dive"`
		Position    uint                       `json:"position"  validate:"numeric"`
		Start       time.Time                  `json:"start"  validate:""`
		End         time.Time                  `json:"end"  validate:""`
	} `json:"questionnaire"`
}
type descriptionUpdateRequest struct {
	Description struct {
		ID              uint   `json:"id" validate:"numeric"`
		QuestionnaireID uint   `json:"questionnaireID" validate:"numeric"`
		Name            string `json:"name" validate:"required,min=1,max=150"`
		Description     string `json:"description" validate:"required,min=1,max=250"`
		ReportTemplate  string `json:"reportTemplate" `
		LanguageID      uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}
type questionUpdateRequest struct {
	Question struct {
		ID                  uint                               `json:"id" validate:"numeric"`
		QuestionnaireID     uint                               `json:"questionnaireID" validate:"numeric"`
		QuestionDescription []questionDescriptionUpdateRequest `json:"description,omitempty" validate:"dive"`
		Answer              []answerUpdateRequest              `json:"answer,omitempty" validate:"dive"`
		TagTypeID           uint                               `json:"tagTypeID" validate:"numeric,gt=0"`
		Position            uint                               `json:"position"  validate:"numeric"`
		StatusID            uint                               `json:"statusID" validate:"numeric,gt=0"`
	} `json:"question"`
}
type questionDescriptionUpdateRequest struct {
	QuestionDescription struct {
		ID          uint   `json:"id" validate:"numeric"`
		QuestionID  uint   `json:"questionID" validate:"numeric"`
		Name        string `json:"name" validate:"required,min=1,max=150"`
		Description string `json:"description" validate:"required,min=1,max=250"`
		LanguageID  uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}
type answerUpdateRequest struct {
	Answer struct {
		ID                uint                             `json:"id" validate:"numeric"`
		QuestionID        uint                             `json:"questionID" validate:"numeric"`
		AnswerDescription []answerDescriptionUpdateRequest `json:"description,omitempty" validate:"dive"`
		Default           bool                             `json:"default"  `
		Position          uint                             `json:"position"  validate:"numeric"`
		Cost              float64                          `json:"cost,string"  validate:"numeric"`
		StatusID          uint                             `json:"statusID" validate:"numeric,gt=0"`
	} `json:"answer"`
}
type answerDescriptionUpdateRequest struct {
	AnswerDescription struct {
		ID          uint   `json:"id" validate:"numeric"`
		AnswerID    uint   `json:"answerID" validate:"numeric"`
		Name        string `json:"name" validate:"required,min=1,max=150"`
		Description string `json:"description" validate:"required,min=1,max=250"`
		LanguageID  uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}

func (r *questionnaireUpdateRequest) bind(c echo.Context, mQuestionnaire *model.Questionnaire) error {
	if err := c.Bind(r); err != nil {
		return err
	}

	if err := c.Validate(r); err != nil {
		return err
	}
	mQuestionnaire.ID = r.Questionnaire.ID
	mQuestionnaire.CreatedAt = time.Now()
	mQuestionnaire.UserID = r.Questionnaire.UserID
	mQuestionnaire.Importance = r.Questionnaire.Importance
	mQuestionnaire.Start = r.Questionnaire.Start
	mQuestionnaire.End = r.Questionnaire.End
	mQuestionnaire.Position = r.Questionnaire.Position
	mQuestionnaire.StatusID = r.Questionnaire.StatusID
	if r.Questionnaire.Description != nil {
		for _, s := range r.Questionnaire.Description {
			// mQuestionnaire.Description = append(mQuestionnaire.Description, model.Description{
			// 	Model:           gorm.Model{ID: s.Description.ID},
			// 	QuestionnaireID: s.Description.QuestionnaireID,
			// 	Name:            s.Description.Name,
			// 	Description:     s.Description.Description,
			// 	LanguageID:      s.Description.LanguageID,
			// })
			var dr model.Description
			dr.CreatedAt = time.Now()
			dr.Model.ID = s.Description.ID
			dr.QuestionnaireID = s.Description.QuestionnaireID
			dr.Name = s.Description.Name
			dr.Description = s.Description.Description
			dr.ReportTemplate = s.Description.ReportTemplate
			dr.LanguageID = s.Description.LanguageID
			mQuestionnaire.Description = append(mQuestionnaire.Description, dr)
		}
	}

	if r.Questionnaire.Question != nil {
		for _, q := range r.Questionnaire.Question {
			var mQuestion model.Question
			mQuestion.ID = q.Question.ID
			mQuestion.CreatedAt = time.Now()
			mQuestion.QuestionnaireID = q.Question.QuestionnaireID
			mQuestion.TagTypeID = q.Question.TagTypeID
			mQuestion.Position = q.Question.Position
			mQuestion.StatusID = q.Question.StatusID

			if q.Question.QuestionDescription != nil {
				for _, qD := range q.Question.QuestionDescription {
					// mQuestion.QuestionDescription = append(mQuestion.QuestionDescription, model.QuestionDescription{
					// 	// Model:       gorm.Model{ID: qD.QuestionDescription.ID},
					// 	// QuestionID:  qD.QuestionDescription.QuestionID,
					// 	Name:        qD.QuestionDescription.Name,
					// 	Description: qD.QuestionDescription.Description,
					// 	LanguageID:  qD.QuestionDescription.LanguageID,
					// })
					var qdr model.QuestionDescription
					qdr.CreatedAt = time.Now()
					qdr.Model.ID = qD.QuestionDescription.ID
					qdr.QuestionID = qD.QuestionDescription.QuestionID
					qdr.Name = qD.QuestionDescription.Name
					qdr.Description = qD.QuestionDescription.Description
					qdr.LanguageID = qD.QuestionDescription.LanguageID
					mQuestion.QuestionDescription = append(mQuestion.QuestionDescription, qdr)
				}
			}

			if q.Question.Answer != nil {
				for _, a := range q.Question.Answer {
					var mAnswer model.Answer
					mAnswer.ID = a.Answer.ID
					mAnswer.QuestionID = a.Answer.QuestionID
					mAnswer.CreatedAt = time.Now()
					mAnswer.Default = a.Answer.Default
					mAnswer.Position = a.Answer.Position
					mAnswer.Cost = a.Answer.Cost
					mAnswer.StatusID = a.Answer.StatusID
					if a.Answer.AnswerDescription != nil {
						for _, aD := range a.Answer.AnswerDescription {
							// mAnswer.AnswerDescription = append(mAnswer.AnswerDescription, model.AnswerDescription{
							// 	// Model:       gorm.Model{ID: aD.AnswerDescription.ID},
							// 	// AnswerID:    aD.AnswerDescription.AnswerID,
							// 	Name:        aD.AnswerDescription.Name,
							// 	Description: aD.AnswerDescription.Description,
							// 	LanguageID:  aD.AnswerDescription.LanguageID,
							// })

							var adr model.AnswerDescription
							adr.CreatedAt = time.Now()
							adr.Model.ID = aD.AnswerDescription.ID
							adr.AnswerID = aD.AnswerDescription.AnswerID
							adr.Name = aD.AnswerDescription.Name
							adr.Description = aD.AnswerDescription.Description
							adr.LanguageID = aD.AnswerDescription.LanguageID
							mAnswer.AnswerDescription = append(mAnswer.AnswerDescription, adr)
						}
					}
					mQuestion.Answer = append(mQuestion.Answer, mAnswer)
				}

			}
			mQuestionnaire.Question = append(mQuestionnaire.Question, mQuestion)
		}
	}
	return nil
}

type createAnswerUserRequest struct {
	UserID          uint                          `json:"userID" validate:"numeric"`
	StatusID        uint                          `json:"statusID"`
	QuestionnaireID uint                          `json:"questionnaireID" validate:"numeric"`
	AnswerUserInfo  []createAnswerUserInfoRequest `json:"answerUserInfo" validate:"dive"`
}
type createAnswerUserInfoRequest struct {
	AnswerUserInfo struct {
		AnswerUserID uint   `json:"answerUserID" validate:"numeric"`
		QuestionID   uint   `json:"questionID" validate:"required,numeric"`
		AnswerID     uint   `json:"answerID" validate:"numeric"`
		AnswerFree   string `json:"answerFree" validate:""`
	} `json:"answerUserInfo"`
}

func (r *createAnswerUserRequest) bind(c echo.Context, mAnswerUser *model.AnswerUser) (err error) {
	// x, _ := ioutil.ReadAll(c.Request().Body)
	// fmt.Printf("-------------- %s", string(x))

	if err = c.Bind(r); err != nil {
		return err
	}

	if err = c.Validate(r); err != nil {
		return err
	}
	mAnswerUser.QuestionnaireID = r.QuestionnaireID
	if r.AnswerUserInfo != nil {
		for _, au := range r.AnswerUserInfo {
			mAnswerUser.AnswerUserInfo = append(mAnswerUser.AnswerUserInfo, model.AnswerUserInfo{
				QuestionID: au.AnswerUserInfo.QuestionID,
				AnswerFree: au.AnswerUserInfo.AnswerFree,
				AnswerID:   au.AnswerUserInfo.AnswerID,
			})
		}
	}

	return
}

type updateAnswerUserRequest struct {
	ID              uint                          `json:"id" validate:"numeric"`
	UserID          uint                          `json:"userID" validate:"numeric"`
	StatusID        uint                          `json:"statusID"`
	QuestionnaireID uint                          `json:"questionnaireID" validate:"numeric"`
	AnswerUserInfo  []updateAnswerUserInfoRequest `json:"answerUserInfo" validate:"dive"`
}
type updateAnswerUserInfoRequest struct {
	AnswerUserInfo struct {
		ID           uint   `json:"id" validate:"numeric"`
		AnswerUserID uint   `json:"answerUserID" validate:"numeric"`
		QuestionID   uint   `json:"questionID" validate:"required,numeric"`
		AnswerID     uint   `json:"answerID" validate:"numeric"`
		AnswerFree   string `json:"answerFree" validate:""`
	} `json:"answerUserInfo"`
}

func (r *updateAnswerUserRequest) bind(c echo.Context, mAnswerUser *model.AnswerUser) (err error) {
	// x, _ := ioutil.ReadAll(c.Request().Body)
	// fmt.Printf("-------------- %s", string(x))

	if err = c.Bind(r); err != nil {
		return err
	}

	if err = c.Validate(r); err != nil {
		return err
	}
	mAnswerUser.ID = r.ID
	mAnswerUser.QuestionnaireID = r.QuestionnaireID
	if r.AnswerUserInfo != nil {
		for _, au := range r.AnswerUserInfo {
			mAnswerUser.AnswerUserInfo = append(mAnswerUser.AnswerUserInfo, model.AnswerUserInfo{
				ID:         au.AnswerUserInfo.ID,
				QuestionID: au.AnswerUserInfo.QuestionID,
				AnswerFree: au.AnswerUserInfo.AnswerFree,
				AnswerID:   au.AnswerUserInfo.AnswerID,
			})
		}
	}

	return
}
