package handlers

import (
	"time"
	"udhtu-api/app/packages/questionnaire/model"
	"udhtu-api/app/packages/questionnaire/usecases/interfaces"

	"github.com/labstack/echo"
)

type descriptionResponse struct {
	Description struct {
		ID              uint   `json:"id" validate:"numeric"`
		QuestionnaireID uint   `json:"questionnaireID" validate:"numeric"`
		Name            string `json:"name" validate:"required,min=1,max=150"`
		Description     string `json:"description" validate:"required,min=1,max=250"`
		ReportTemplate  string `json:"reportTemplate" `
		LanguageID      uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}
type questionResponse struct {
	Question struct {
		ID                  uint                          `json:"id" validate:"numeric"`
		QuestionnaireID     uint                          `json:"questionnaireID" validate:"numeric"`
		QuestionDescription []questionDescriptionResponse `json:"description" validate:"dive"`
		Answer              []answerResponse              `json:"answer" validate:"dive"`
		TagTypeID           uint                          `json:"tagTypeID" validate:"numeric,gt=0"`
		Position            uint                          `json:"position"  validate:"numeric"`
		StatusID            uint                          `json:"statusID" validate:"numeric,gt=0"`
	} `json:"question"`
}
type questionDescriptionResponse struct {
	QuestionDescription struct {
		ID          uint   `json:"id" validate:"numeric"`
		QuestionID  uint   `json:"questionID" validate:"numeric"`
		Name        string `json:"name" validate:"required,min=1,max=150"`
		Description string `json:"description" validate:"required,min=1,max=250"`
		LanguageID  uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}
type answerResponse struct {
	Answer struct {
		ID                uint                        `json:"id" validate:"numeric"`
		QuestionID        uint                        `json:"questionID" validate:"numeric"`
		AnswerDescription []answerDescriptionResponse `json:"description" validate:"dive"`
		Default           bool                        `json:"default"  `
		Position          uint                        `json:"position"  validate:"numeric"`
		Cost              float64                     `json:"cost,string"  validate:"numeric"`
		StatusID          uint                        `json:"statusID" validate:"numeric,gt=0"`
	} `json:"answer"`
}
type answerDescriptionResponse struct {
	AnswerDescription struct {
		ID          uint   `json:"id" validate:"numeric"`
		AnswerID    uint   `json:"answerID" validate:"numeric"`
		Name        string `json:"name" validate:"required,min=1,max=150"`
		Description string `json:"description" validate:"required,min=1,max=250"`
		LanguageID  uint   `json:"languageID" validate:"numeric,gt=0"`
	} `json:"description"`
}
type questionnaireResponse struct {
	ID          uint                  `json:"id" validate:"numeric"`
	UserID      uint                  `json:"userID" validate:"numeric"`
	Importance  uint                  `json:"importance" validate:"numeric"`
	StatusID    uint                  `json:"statusID" validate:"required,numeric,gt=0"`
	Description []descriptionResponse `json:"description" validate:"dive"`
	Question    []questionResponse    `json:"question" validate:"dive"`
	Position    uint                  `json:"position"  validate:"numeric"`
	Start       time.Time             `json:"start"  validate:""`
	End         time.Time             `json:"end"  validate:""`
}

type singlQuestionnaireResponse struct {
	Questionnaire *questionnaireResponse `json:"questionnaire"`
}

type questionnaireListResponse struct {
	Questionnaire       []model.Questionnaire `json:"questionnaire"`
	QuestionnairesCount int                   `json:"total"`
}

func newQuestionnaireResponse(c echo.Context, mQuestionnaire *model.Questionnaire) *singlQuestionnaireResponse {
	qr := new(questionnaireResponse)
	qr.ID = mQuestionnaire.ID
	qr.Importance = mQuestionnaire.Importance
	qr.StatusID = mQuestionnaire.StatusID
	qr.Position = mQuestionnaire.Position
	qr.UserID = mQuestionnaire.UserID
	qr.Start = mQuestionnaire.Start
	qr.End = mQuestionnaire.End
	qr.Question = make([]questionResponse, 0)
	qr.Description = make([]descriptionResponse, 0)
	if mQuestionnaire.Description != nil {
		for _, qd := range mQuestionnaire.Description {
			// qr.Description = append(qr.Description, descriptionResponse{
			// 	ID:              qd.ID,
			// 	QuestionnaireID: qd.QuestionnaireID,
			// 	Name:            qd.Name,
			// 	Description:     qd.Description,
			// 	ReportTemplate:  qd.ReportTemplate,
			// 	LanguageID:      qd.LanguageID,
			// })
			var dr descriptionResponse
			dr.Description.ID = qd.ID
			dr.Description.QuestionnaireID = qd.QuestionnaireID
			dr.Description.Name = qd.Name
			dr.Description.Description = qd.Description
			dr.Description.ReportTemplate = qd.ReportTemplate
			dr.Description.LanguageID = qd.LanguageID
			qr.Description = append(qr.Description, dr)
		}
	}

	if mQuestionnaire.Question != nil {
		for _, q := range mQuestionnaire.Question {
			var mQuestion questionResponse
			mQuestion.Question.ID = q.ID
			mQuestion.Question.QuestionnaireID = q.QuestionnaireID
			mQuestion.Question.TagTypeID = q.TagTypeID
			mQuestion.Question.Position = q.Position
			mQuestion.Question.StatusID = q.StatusID
			mQuestion.Question.QuestionDescription = make([]questionDescriptionResponse, 0)
			mQuestion.Question.Answer = make([]answerResponse, 0)
			if q.QuestionDescription != nil {
				for _, qD := range q.QuestionDescription {
					// mQuestion.QuestionDescription = append(mQuestion.QuestionDescription, questionDescriptionResponse{
					// 	Name:        qD.Name,
					// 	Description: qD.Description,
					// 	LanguageID:  qD.LanguageID,
					// })
					var qdr questionDescriptionResponse
					// qdr.QuestionDescription.QuestionnaireID = qd.QuestionnaireID
					qdr.QuestionDescription.ID = qD.ID
					qdr.QuestionDescription.QuestionID = qD.QuestionID
					qdr.QuestionDescription.Name = qD.Name
					qdr.QuestionDescription.Description = qD.Description
					qdr.QuestionDescription.LanguageID = qD.LanguageID
					mQuestion.Question.QuestionDescription = append(mQuestion.Question.QuestionDescription, qdr)
				}
			}

			if q.Answer != nil {
				for _, a := range q.Answer {
					var mAnswer answerResponse
					mAnswer.Answer.ID = a.ID
					mAnswer.Answer.QuestionID = a.QuestionID
					mAnswer.Answer.Default = a.Default
					mAnswer.Answer.Position = a.Position
					mAnswer.Answer.Cost = a.Cost
					mAnswer.Answer.StatusID = a.StatusID
					mAnswer.Answer.AnswerDescription = make([]answerDescriptionResponse, 0)
					if a.AnswerDescription != nil {
						for _, aD := range a.AnswerDescription {
							// mAnswer.AnswerDescription = append(mAnswer.AnswerDescription, answerDescriptionResponse{
							// 	Name:        aD.Name,
							// 	Description: aD.Description,
							// 	LanguageID:  aD.LanguageID,
							// })
							var adr answerDescriptionResponse
							adr.AnswerDescription.ID = aD.ID
							adr.AnswerDescription.AnswerID = aD.AnswerID
							adr.AnswerDescription.Name = aD.Name
							adr.AnswerDescription.Description = aD.Description
							adr.AnswerDescription.LanguageID = aD.LanguageID
							mAnswer.Answer.AnswerDescription = append(mAnswer.Answer.AnswerDescription, adr)
						}
					}
					mQuestion.Question.Answer = append(mQuestion.Question.Answer, mAnswer)
				}

			}

			qr.Question = append(qr.Question, mQuestion)
		}

	}

	return &singlQuestionnaireResponse{qr}
}

func newQuestionnaireListResponse(us interfaces.Usecase, userID uint, mQuestionnaire []model.Questionnaire, count int) *questionnaireListResponse {
	qrList := new(questionnaireListResponse)
	qrList.Questionnaire = mQuestionnaire
	qrList.QuestionnairesCount = count
	return qrList
}

type answersUserResponse struct {
	ID              uint                     `json:"id" validate:"required,numeric"`
	UserID          uint                     `json:"userID" validate:"numeric"`
	CreatedAt       time.Time                `json:"createdAt"  validate:""`
	UpdatedAt       time.Time                `json:"updatedAt"  validate:""`
	StatusID        uint                     `json:"statusID"`
	QuestionnaireID uint                     `json:"questionnaireID" validate:"numeric"`
	AnswerUserInfo  []answerUserInfoResponse `json:"answerUserInfo" validate:"dive"`
}
type answerUserInfoResponse struct {
	AnswerUserInfo struct {
		ID           uint   `json:"id" validate:"required,numeric"`
		AnswerUserID uint   `json:"answerUserID" validate:"numeric"`
		QuestionID   uint   `json:"questionID" validate:"required,numeric"`
		AnswerID     uint   `json:"answerID" validate:"numeric"`
		AnswerFree   string `json:"answerFree" validate:""`
	} `json:"answerUserInfo"`
}

func newAnswersUserResponse(c echo.Context, mAnswerUser *model.AnswerUser) *answersUserResponse {
	aur := new(answersUserResponse)
	// fmt.Printf("[[[[[[[[[[]]]]]]]]]] %+v", mAnswerUser)
	aur.ID = mAnswerUser.ID
	aur.QuestionnaireID = mAnswerUser.QuestionnaireID
	aur.CreatedAt = mAnswerUser.CreatedAt
	aur.UpdatedAt = mAnswerUser.UpdatedAt
	aur.AnswerUserInfo = make([]answerUserInfoResponse, 0)
	if mAnswerUser.AnswerUserInfo != nil {
		for _, au := range mAnswerUser.AnswerUserInfo {
			var auiR answerUserInfoResponse
			auiR.AnswerUserInfo.ID = au.ID
			auiR.AnswerUserInfo.AnswerUserID = au.AnswerUserID
			auiR.AnswerUserInfo.QuestionID = au.QuestionID
			auiR.AnswerUserInfo.AnswerID = au.AnswerID
			auiR.AnswerUserInfo.AnswerFree = au.AnswerFree
			aur.AnswerUserInfo = append(aur.AnswerUserInfo, auiR)

		}
	}

	return aur
}
