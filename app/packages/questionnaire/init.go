package questionnaire

import (
	"fmt"
	config "udhtu-api/app/packages/questionnaire/config/interfaces"
	"udhtu-api/app/packages/questionnaire/handlers"
	repositories "udhtu-api/app/packages/questionnaire/repositories/gorm"
	"udhtu-api/app/packages/questionnaire/usecases"

	"github.com/jinzhu/gorm"
)

type (
	Questionnaire struct {
		*handlers.Handler
	}
)

func New(db *gorm.DB) (*Questionnaire, error) {
	cfg := config.GetConfig()
	// if err != nil {
	// 	fmt.Println("Init Questionnaire config err", err)
	// }
	rep, err := repositories.New(db, cfg)
	if err != nil {
		fmt.Println("Init Questionnaire repositories err", err)
	}
	usc := usecases.New(rep)
	// if err != nil {
	// 	fmt.Println("Init Questionnaire Usecases err", err)
	// }
	// return &Questionnaire{usc}
	return &Questionnaire{handlers.NewHandler(usc)}, err
}
