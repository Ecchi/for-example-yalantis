package model

import (
	"time"
	config "udhtu-api/app/packages/questionnaire/config/interfaces"

	"github.com/jinzhu/gorm"
)

type (
	Status struct {
		ID   uint   `gorm:"primary_key;AUTO_INCREMENT"`
		Name string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
	}
	TagType struct {
		ID       int    `gorm:"primary_key;AUTO_INCREMENT"`
		Name     string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
		StatusID uint   `gorm:"type:int unsigned;index;not null" `
	}

	Language struct {
		ID       uint   `gorm:"primary_key;AUTO_INCREMENT"`
		Name     string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
		Code     string `gorm:"type:varchar(10)" validate:"required,min=1,max=10"`
		StatusID uint   `gorm:"type:int unsigned;index;not null" `
	}
	Questionnaire struct {
		gorm.Model
		Importance  uint          `gorm:"type:int unsigned;index;not null" `
		UserID      uint          `gorm:"type:int unsigned;index;not null"`
		Start       time.Time     `gorm:"type:datetime"`
		End         time.Time     `gorm:"type:datetime"`
		StatusID    uint          `gorm:"type:int unsigned;index;not null" `
		OneTime     bool          `gorm:"type:boolean;not null" `
		Description []Description //`gorm:"foreignkey:id;association_foreignkey:questionnaire_id"`
		Question    []Question    `validate:"dive"` //`gorm:"foreignkey:id;association_foreignkey:questionnaire_id"`
		Position    uint          `gorm:"type:int unsigned" validate:"numeric,gt=0"`
	}
	Description struct {
		gorm.Model
		Name            string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
		Description     string `gorm:"type:varchar(250);not null" validate:"required,min=10,max=250"`
		QuestionnaireID uint   `gorm:"type:int unsigned;index;not null" `
		ReportTemplate  string `json:"report_template" `
		LanguageID      uint   `gorm:"type:int unsigned;index;not null" `
	}

	Question struct {
		gorm.Model
		QuestionnaireID uint `gorm:"type:int unsigned;index;not null" `
		// input - ручной ответ select/checkbox - выбор/Порядок приоритета - ??
		QuestionDescription []QuestionDescription `validate:"dive"` //`gorm:"foreignkey:id;association_foreignkey:id"`
		Answer              []Answer              `validate:"dive"`
		TagTypeID           uint                  `gorm:"type:int unsigned;index;not null" `
		Position            uint                  `gorm:"type:int unsigned" validate:"numeric,gt=0"`
		StatusID            uint                  `gorm:"type:int unsigned;index;not null" `
	}
	QuestionDescription struct {
		gorm.Model
		Name        string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
		Description string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
		LanguageID  uint   `gorm:"type:int unsigned;index;not null" `
		QuestionID  uint   `gorm:"type:int unsigned;index;not null" `
	}

	Answer struct {
		gorm.Model
		AnswerDescription []AnswerDescription `validate:"dive"`
		QuestionID        uint                `gorm:"type:int unsigned;index;not null" `
		Default           bool                `gorm:"type:boolean;not null" `
		Position          uint                `gorm:"type:int unsigned" validate:"numeric,gt=10"`
		Cost              float64             `gorm:"type:decimal(10,2); not null"`
		StatusID          uint                `gorm:"type:int unsigned;index;not null" `
	}
	AnswerDescription struct {
		gorm.Model
		Name        string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
		AnswerID    uint   `gorm:"type:int unsigned;index;not null" `
		Description string `gorm:"type:varchar(150);not null" validate:"required,min=1,max=150"`
		LanguageID  uint   `gorm:"type:int unsigned;index;not null" `
	}
	AnswerUser struct {
		gorm.Model
		UserID          uint             `gorm:"type:int unsigned;index;not null" `
		AnswerUserInfo  []AnswerUserInfo `validate:"dive"`
		QuestionnaireID uint             `gorm:"type:int unsigned;index;not null" `
		StatusID        uint             `gorm:"type:int unsigned;index;not null" `
	}
	AnswerUserInfo struct {
		ID uint `gorm:"primary_key;AUTO_INCREMENT"`
		// UserID          uint   `gorm:"type:int unsigned;index;not null" `
		// QuestionnaireID uint   `gorm:"type:int unsigned;index;not null" `
		AnswerUserID uint   `gorm:"type:int unsigned;index;not null" `
		QuestionID   uint   `gorm:"type:int unsigned;index;not null" `
		AnswerID     uint   `gorm:"type:int unsigned" `
		AnswerFree   string `gorm:"type:text"`
	}
)

func (Status) TableName() string {
	return config.GetConfig().Package.TablePrefix + "status"
}
func (TagType) TableName() string {
	return config.GetConfig().Package.TablePrefix + "tag_types"
}
func (Language) TableName() string {
	return config.GetConfig().Package.TablePrefix + "languages"
}

// func (Questionnaire) TableName() string {
// 	return config.GetConfig().Package.TablePrefix + "questionnaires"
// }
func (Description) TableName() string {
	return config.GetConfig().Package.TablePrefix + "descriptions"
}

func (Question) TableName() string {
	return config.GetConfig().Package.TablePrefix + "questions"
}

func (QuestionDescription) TableName() string {
	return config.GetConfig().Package.TablePrefix + "question_descriptions"
}

func (Answer) TableName() string {
	return config.GetConfig().Package.TablePrefix + "answers"
}

func (AnswerDescription) TableName() string {
	return config.GetConfig().Package.TablePrefix + "answer_descriptions"
}

func (AnswerUser) TableName() string {
	return config.GetConfig().Package.TablePrefix + "answer_users"
}
func (AnswerUserInfo) TableName() string {
	return config.GetConfig().Package.TablePrefix + "answer_user_info"
}
