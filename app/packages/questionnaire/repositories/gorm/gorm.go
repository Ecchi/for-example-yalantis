package gorm

import (
	"fmt"
	"udhtu-api/app/packages/questionnaire/model"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

func (repo *Repository) CreateQuestionnaire(questionnaire *model.Questionnaire) (err error) {
	err = repo.DB.Create(questionnaire).Error
	if err != nil {
		return errors.Errorf("questionnaire небыл создан - %s", err)
	}
	return
}

func (repo *Repository) GetAnswersByUserIDByQuestionnaireID(au *model.AnswerUser) (*model.AnswerUser, error) {
	var answerUser model.AnswerUser
	repo.DB.LogMode(true)
	err := repo.DB.Debug().Set("gorm:auto_preload", true).Where(&au).Find(&answerUser).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, errors.Errorf("Возникла ошибка репозитария GetAnswersByUserIDByQuestionnaireID - %s", err)
	}
	return &answerUser, nil
}
func (repo *Repository) GetQuestionnaires(q *model.Questionnaire) (questionnaires []model.Questionnaire, err error) {
	// err = repo.DB.Where(&q).Preload("Description").Preload("Question").Find(&questionnaires).Error
	err = repo.DB.Set("gorm:auto_preload", true).Where(&q).Find(&questionnaires).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, errors.Errorf("Возникла ошибка репозитария GetQuestionnaires - %s", err)
	}
	return
}
func (repo *Repository) GetQuestionnaireByID(q *model.Questionnaire) (*model.Questionnaire, error) {
	// err = repo.DB.Where(&q).Preload("Description").Preload("Question").Find(&questionnaires).Error
	var questionnaire model.Questionnaire
	if err := repo.DB.Set("gorm:auto_preload", true).Where(q).First(&questionnaire).Error; err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, nil
		}
		return nil, err
	}
	return &questionnaire, nil
}
func (repo *Repository) GetListQuestionnaires(offset, limit int) ([]model.Questionnaire, int, error) {
	var (
		questionnaires []model.Questionnaire
		count          int
		err            error
	)
	repo.DB.Model(&questionnaires).Where(&model.Questionnaire{StatusID: 1}).Count(&count)
	err = repo.DB.Preload("Description").Where(&model.Questionnaire{StatusID: 1}).Offset(offset).Limit(limit).Order("created_at desc").Find(&questionnaires).Error
	return questionnaires, count, err
}

func (repo *Repository) CreateQuestion(question *model.Question) (err error) {
	err = repo.DB.Create(question).Error
	if err != nil {
		return errors.Errorf("question небыл создан - %s", err)
	}
	return
}
func (repo *Repository) UpdateQuestionnaire(questionnaire *model.Questionnaire) (err error) {
	// err = repo.DB.Model(question).Association("Description").Replace("Description").Error
	fmt.Printf("%+v", questionnaire)
	err = repo.DB.Save(questionnaire).Error
	if err != nil {
		return errors.Errorf("questionnaire небыл обновлен - %s", err)
	}
	return
}

func (repo *Repository) GetQuestions(question *model.Question) (questions []model.Question, err error) {
	err = repo.DB.Where(question).Find(&questions).Error
	if err != nil {
		return nil, errors.Errorf("Возникла ошибка репозитария question - %s", err)
	}
	return
}
func (repo *Repository) CreateTagType(tagType *model.TagType) (err error) {
	err = repo.DB.Create(tagType).Error
	if err != nil {
		return errors.Errorf("questionType небыл создан - %s", err)
	}
	return
}

func (repo *Repository) GetTagTypes(questionTagType *model.TagType) (questionTagTypes []model.TagType, err error) {
	err = repo.DB.Where(&questionTagType).Find(&questionTagTypes).Error
	if err != nil {
		return nil, errors.Errorf("Возникла ошибка репозитария questionTagTypes - %s", err)
	}
	return
}

func (repo *Repository) CreateAnswer(answer *model.Answer) (err error) {
	err = repo.DB.Create(answer).Error
	if err != nil {
		return errors.Errorf("answer небыл создан - %s", err)
	}
	return
}
func (repo *Repository) UpdateAnswer(au *model.AnswerUser) (err error) {
	err = repo.DB.Model(&model.AnswerUser{Model: gorm.Model{ID: au.ID}}).Association("AnswerUserInfo").Replace(&au.AnswerUserInfo).Error
	if err != nil {
		return errors.Errorf("UpdateAnswer небыл upd - %s", err)
	}
	return
}

func (repo *Repository) GetAnswers(answer *model.Answer) (answers []model.Answer, err error) {
	err = repo.DB.Where(answer).Find(&answers).Error
	if err != nil {
		return nil, errors.Errorf("Возникла ошибка репозитария GetQuestionAnswers - %s", err)
	}
	return
}
func (repo *Repository) CreateAnswerUser(answerUser *model.AnswerUser) (err error) {
	err = repo.DB.Create(answerUser).Error
	if err != nil {
		return errors.Errorf("answerUser небыл создан - %s", err)
	}
	return
}

// func (repo *Repository) CreateAnswerUser(answerUser []model.AnswerUser) (err error) {
// 	tx := repo.DB.Begin()
// 	for _, au := range answerUser {
// 		err = tx.Create(&au).Error
// 		if err != nil {
// 			tx.Rollback()
// 			return errors.Errorf("answersUser небыл создан - %s", err)
// 		}
// 	}
// 	return tx.Commit().Error
// }

// // ---
// func (repo *Repository) GetAccount(findInvoice  model.Invoice) (account  model.Account, err error) {
// 	// err = repo.DB.Table("transactions").Select("user_id,sum(amount) as amount").Where("user_id = ?", userID).Find(&account).Error
// 	// err = repo.DB.Model(&tr).Related(&invoice).Error
// 	// err = repo.DB.Table("transactions").Select("user_id,sum(amount) as amount,invoice_id").Where("invoice_id = ?", invoiceID).Preload("Invoice").Find(&account).Error
// 	err = repo.DB.Table("transactions").Select("sum(amount) as amount,invoice_id").Where("invoice_id = ?", repo.DB.Table("invoices").Select("id").Where(&findInvoice).SubQuery()).Preload("Invoice").Find(&account).Error
// 	if err != nil {
// 		return account, errors.Errorf("Возникла ошибка репозитария GetAccountByInvoiceID - %s", err)
// 	}
// 	if account.Invoice.ID <= 0 {
// 		return account, errors.New("InvoiceID не найден в таблице транзакций")
// 	}

// 	return
// }

// // Трансвер средст со счета на счет
// func (repo *Repository) RegisterTransferTransaction(fromTransaction  model.Transaction, toTransaction  model.Transaction) (err error) {
// 	tx := repo.DB.Begin()
// 	defer func() {
// 		if r := recover(); r != nil {
// 			tx.Rollback()
// 		}
// 	}()
// 	if tx.Error != nil {
// 		return err
// 	}
// 	if err := tx.Create(&fromTransaction).Error; err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	if err := tx.Create(&toTransaction).Error; err != nil {
// 		tx.Rollback()
// 		return err
// 	}
// 	return tx.Commit().Error
// }

// // Изменить статус счета активен/неактивен/блокирован и т д
// func (repo *Repository) ChangeInvoiceStatus(invoiceDTO  model.ChangeInvoiceStatusDTO) (invoice  model.Invoice, err error) {
// 	err = repo.DB.Model(&invoice).Where("id = ?", invoiceDTO.ID).Update("namstatus_ide", invoiceDTO.StatusID).Error
// 	// err = repo.DB.Model(&invoice).Where("id = ?", invoiceID).Update("namstatus_ide", statusID).Error
// 	if err != nil {
// 		if gorm.IsRecordNotFoundError(err) {
// 			return invoice, err
// 		}
// 		return invoice, errors.Errorf("Ошибка при изменение ChangeInvoiceStatus статуса  - %s", err)
// 	}
// 	return

// }
