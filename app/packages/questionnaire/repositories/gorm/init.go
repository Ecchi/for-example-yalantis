package gorm

import (
	config "udhtu-api/app/packages/questionnaire/config/interfaces"
	"udhtu-api/app/packages/questionnaire/model"

	"github.com/jinzhu/gorm"
)

type (
	Repository struct {
		DB *gorm.DB
	}
)

// Создает при старте приложения соеденение , !! - если при старте не отработала повторного вызова не будет!!!!!
func New(db *gorm.DB, cfg *config.Configuration) (*Repository, error) {
	var err error
	// gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
	// 	return "prefix_" + defaultTableName
	// }
	//Drop table
	// db.DropTableIfExists(&model.Status{}, &model.TagType{})
	err = db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(
		&model.Status{},
		&model.TagType{},
		&model.Language{},
		&model.Questionnaire{},
		&model.Description{},
		&model.Question{},
		&model.QuestionDescription{},
		&model.Answer{},
		&model.AnswerDescription{},
		&model.AnswerUser{},
		&model.AnswerUserInfo{},
	).Error
	//We need to add foreign keys manually.
	err = db.Model(&model.Questionnaire{}).
		AddForeignKey("status_id", cfg.Package.TablePrefix+"status(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.Description{}).
		AddForeignKey("questionnaire_id", "questionnaires(id)", "CASCADE", "CASCADE").
		AddForeignKey("language_id", cfg.Package.TablePrefix+"languages(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.TagType{}).
		AddForeignKey("status_id", cfg.Package.TablePrefix+"status(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.Language{}).
		AddForeignKey("status_id", cfg.Package.TablePrefix+"status(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.Question{}).
		AddForeignKey("status_id", cfg.Package.TablePrefix+"status(id)", "RESTRICT", "RESTRICT").
		AddForeignKey("tag_type_id", cfg.Package.TablePrefix+"tag_types(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.QuestionDescription{}).
		AddForeignKey("question_id", cfg.Package.TablePrefix+"questions(id)", "CASCADE", "CASCADE").
		AddForeignKey("language_id", cfg.Package.TablePrefix+"languages(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.Answer{}).
		AddForeignKey("question_id", cfg.Package.TablePrefix+"questions(id)", "CASCADE", "CASCADE").
		AddForeignKey("status_id", cfg.Package.TablePrefix+"status(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.AnswerDescription{}).
		AddForeignKey("answer_id", cfg.Package.TablePrefix+"answers(id)", "CASCADE", "CASCADE").
		AddForeignKey("language_id", cfg.Package.TablePrefix+"languages(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.AnswerUser{}).
		AddForeignKey("status_id", cfg.Package.TablePrefix+"status(id)", "RESTRICT", "RESTRICT").
		AddForeignKey("questionnaire_id", "questionnaires(id)", "RESTRICT", "RESTRICT").
		Error
	err = db.Model(&model.AnswerUserInfo{}).
		AddForeignKey("question_id", cfg.Package.TablePrefix+"questions(id)", "RESTRICT", "RESTRICT").
		// AddForeignKey("answer_id", cfg.Package.TablePrefix+"answers(id)", "RESTRICT", "RESTRICT").
		AddForeignKey("answer_user_id", cfg.Package.TablePrefix+"answer_users(id)", "RESTRICT", "RESTRICT").
		Error

	// var q model.Questionnaire
	// q.StatusID = 1
	// q.Importance = 1
	// q.UserID = 1
	// q.Position = 1
	// // Des

	// q.Description = append(q.Description, model.Description{
	// 	Name:           "Description1",
	// 	LanguageID:     1,
	// 	ReportTemplate: "Text",
	// })

	// q.Question = append(q.Question, model.Question{
	// 	TagTypeID: 1,
	// 	StatusID:  1,
	// 	Position:  1,
	// 	QuestionDescription: []model.QuestionDescription{
	// 		model.QuestionDescription{Name: "На анлиском", LanguageID: 1, Description: "Im description"},
	// 		model.QuestionDescription{Name: "На руском", LanguageID: 2, Description: "Im description"},
	// 	},
	// 	Answer: []model.Answer{
	// 		model.Answer{
	// 			Default:  false,
	// 			Position: 1,
	// 			Cost:     22,
	// 			StatusID: 1,
	// 			AnswerDescription: []model.AnswerDescription{
	// 				model.AnswerDescription{Name: "На анлиском answer", LanguageID: 1, Description: "Im description"},
	// 				model.AnswerDescription{Name: "На руском answer", LanguageID: 2, Description: "Im description"},
	// 			},
	// 		},
	// 	},
	// })
	// err = db.Create(&q).Error
	// fmt.Printf("qq  %+v", q)
	// err = db.Create(&model.Status{Name: "enable"}).Error
	// err = db.Create(&model.Language{Name: "Ukraine", StatusID: 1}).Error
	// err = db.Create(&model.TagType{Name: "input", StatusID: 1}).Error
	// err = db.Create(&q).Error
	// err = db.Set("gorm:save_associations", false).Create(&q).Error
	// var qq []model.Questionnaire
	// var ww model.Questionnaire
	// ww.ID = 1
	// qq.Model.ID = 10
	// err = db.Preload("Description").Preload("Question").Find(&qq).Error
	// err = db.Model(&ww).Association("Question").Append(&model.Question{StatusID: 1, TagTypeID: 1, Position: 20}).Error
	// n := db.Model(&ww).Association("Question").Count()
	// fmt.Printf("----- %+v", q)
	// err = db.Save(&q).Error
	return &Repository{DB: db}, err
}
