package interfaces

import (
	"udhtu-api/app/packages/questionnaire/model"
)

type (
	Repository interface {
		// Questionnaire
		CreateQuestionnaire(*model.Questionnaire) (err error)
		GetQuestionnaires(setQueryFields *model.Questionnaire) (questionnaires []model.Questionnaire, err error)
		GetQuestionnaireByID(*model.Questionnaire) (*model.Questionnaire, error)
		GetListQuestionnaires(offset, limit int) ([]model.Questionnaire, int, error)
		UpdateQuestionnaire(*model.Questionnaire) (err error)
		// QuestionType
		CreateTagType(*model.TagType) (err error)
		GetTagTypes(setQueryFields *model.TagType) (tagTypes []model.TagType, err error)
		// Question
		CreateQuestion(*model.Question) (err error)
		GetQuestions(setQueryFields *model.Question) (questions []model.Question, err error)

		// Answer
		CreateAnswer(*model.Answer) (err error)
		GetAnswers(setQueryFields *model.Answer) (questions []model.Answer, err error)

		GetAnswersByUserIDByQuestionnaireID(*model.AnswerUser) (*model.AnswerUser, error)

		CreateAnswerUser(*model.AnswerUser) (err error)
		UpdateAnswer(*model.AnswerUser) (err error)
	}
)
