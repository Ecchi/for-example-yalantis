package interfaces

import (
	"udhtu-api/app/packages/questionnaire/model"
)

type (
	Usecase interface {
		// Plan
		CreateQuestionnaire(*model.Questionnaire) (err error)
		GetQuestionnaireByID(*model.Questionnaire) (*model.Questionnaire, error)
		GetListQuestionnaires(offset, limit int) ([]model.Questionnaire, int, error)
		UpdateQuestionnaire(*model.Questionnaire) (err error)
		GetAnswersByUserIDByQuestionnaireID(*model.AnswerUser) (*model.AnswerUser, error)
		UpdateAnswer(*model.AnswerUser) (err error)
		// CreatePlanQuestion(planQuestion model.PlanQuestion) (err error)
		// GetPlanQuestions(planQuestion model.PlanQuestion) (planQuestions []model.PlanQuestion, err error)
		// // QuestionType
		// CreateQuestionTagType(questionType model.QuestionTagType) (err error)
		// GetQuestionTagTypes(questionType model.QuestionTagType) (questionTypes []model.QuestionTagType, err error)
		// // Question
		// CreateQuestion(question model.Question) (err error)
		// GetQuestions(question model.Question) (questions []model.Question, err error)
		// // Answer
		// CreateAnswer(plan model.Answer) (err error)
		CreateAnswerUser(*model.AnswerUser) (err error)
	}
)
