package usecases

import (
	"udhtu-api/app/packages/questionnaire/model"
	"udhtu-api/app/packages/questionnaire/repositories/gorm"
	"udhtu-api/app/packages/questionnaire/repositories/interfaces"
)

type (
	Usecase struct {
		interfaces.Repository
	}
)

func New(rep *gorm.Repository) *Usecase {
	return &Usecase{
		rep,
	}

}
func (usecase *Usecase) CreateQuestionnaire(questionnaire *model.Questionnaire) (err error) {
	// config, err := utilites.ReadConfig("core/config/global")
	// if err != nil {
	// 	return transaction, errors.New("Настроики или сам файл с ними data/global отсутсвует")
	// }
	// transaction.TransactionTypeID = config.GetInt("accounting.transaction_type.deposit")

	return usecase.Repository.CreateQuestionnaire(questionnaire)
}

func (usecase *Usecase) UpdateQuestionnaire(questionnaire *model.Questionnaire) (err error) {
	return usecase.Repository.UpdateQuestionnaire(questionnaire)
}
func (usecase *Usecase) CreateAnswerUser(answerUser *model.AnswerUser) (err error) {
	return usecase.Repository.CreateAnswerUser(answerUser)
}
func (usecase *Usecase) GetListQuestionnaires(offset, limit int) ([]model.Questionnaire, int, error) {

	if offset != 0 && offset != 1 {
		offset = offset*limit - limit
	}
	return usecase.Repository.GetListQuestionnaires(offset, limit)
}
func (usecase *Usecase) GetQuestionnaireByID(q *model.Questionnaire) (*model.Questionnaire, error) {

	return usecase.Repository.GetQuestionnaireByID(q)
}
func (usecase *Usecase) GetAnswersByUserIDByQuestionnaireID(au *model.AnswerUser) (*model.AnswerUser, error) {
	return usecase.Repository.GetAnswersByUserIDByQuestionnaireID(au)
}

func (usecase *Usecase) UpdateAnswer(answerUser *model.AnswerUser) (err error) {
	return usecase.Repository.UpdateAnswer(answerUser)
}
