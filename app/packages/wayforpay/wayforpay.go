package wayforpay

import "time"

type (
	// {"merchantAccount":"185_236_78_124",
	// 	"orderReference":"222222",
	// 	"merchantSignature":"649b2d9c928fe48bc4a9767fd99074fb",
	// 	"amount":5,
	// 	"currency":"UAH",
	// 	"authCode":"37171B",
	// 	"email":"mugivar@gmail.com",
	// 	"phone":"380661659858",
	// 	"createdDate":1545088356,
	// 	"processingDate":1545092591,
	// 	"cardPan":"51****3938",
	// 	"cardType":"MasterCard",
	// 	"issuerBankCountry":"Ukraine",
	// 	"issuerBankName":"COMMERCIAL BANK PRIVATBANK",
	// 	"recToken":"",
	// 	"transactionStatus":"Pending",
	// 	"reason":"Transaction is pending",
	// 	"reasonCode":1134,
	// 	"fee":0,
	// 	"paymentSystem":"card",
	// 	"cardProduct":"debit",
	// 	"clientName":"NIKALAS KRASKO"}
	Wayforpay struct {
		MerchantAccount   string
		OrderReference    string
		MerchantSignature string
		Amount            float64 `gorm:"column:amount;type:decimal(10,2); not null"`
		Currency          string
		AuthCode          string
		Email             string
		Phone             string
		CreatedDate       time.Time
		ProcessingDate    time.Time
		CardPan           string
		CardType          string
		IssuerBankCountry string
		IssuerBankName    string
		RecToken          string
		TransactionStatus string
		Reason            string
		ReasonCode        int64
		Fee               int64
		PaymentSystem     string
		CardProduct       string
		ClientName        string
	}
	WayforpayRepository interface {
	}

	WayforpayUsecase interface {
	}
)
