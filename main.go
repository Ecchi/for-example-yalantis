package main

import (
	config "udhtu-api/app/config/interfaces"
	"udhtu-api/app/handlers"
	"udhtu-api/app/helpers"

	"udhtu-api/app/packages/questionnaire"

	"udhtu-api/app/packages/authentification"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"go.uber.org/dig"
)

func BuildContainer() *dig.Container {
	container := dig.New()
	// Server init
	container.Provide(helpers.GetConfig)
	container.Provide(helpers.ConnectDB)
	// Questionnaire
	container.Provide(questionnaire.New)
	// Authentification
	container.Provide(authentification.New)
	// init all with handlers
	container.Provide(handlers.NewHandler)
	return container
}

func main() {
	container := BuildContainer()

	err := container.Invoke(func(init *handlers.Handler, gormDb *gorm.DB, config *config.Configuration) {
		defer gormDb.Close()
		v1 := init.Echo.Group("/api/v1")
		init.Register(v1)
		// fmt.Println("------", string(config.Server.Port))
		init.Echo.Logger.Fatal(init.Echo.Start(":" + config.Server.Port))
	})
	if err != nil {
		panic(err)
	}
}
